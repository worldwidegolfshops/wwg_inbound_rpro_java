package com.wwgs.dataprocessor.inbound;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.commons.lang3.StringUtils;

import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.CSVWriter;
import com.opencsv.RFC4180Parser;
import com.opencsv.RFC4180ParserBuilder;

public class RproReportProcessor {

	public static void main(String[] args) throws FileNotFoundException {
		String inboundRPROFile = "C:\\Eclipse\\WWG\\akeneoCompare\\RPROTIMES_IMPORT.csv";
		String inboundAkeneoFile = "C:\\Eclipse\\WWG\\akeneoCompare\\AKENEO_EXPORT.csv";
		String outboundFolder = "C:\\Eclipse\\WWG\\akeneoCompare\\";
	
		loadRproData(inboundRPROFile);
	}


	public static void loadRproData(String inboundRPROFile) {
		String delimiter = "|";
		

		List<String> headers = new ArrayList<String>();
		HashMap rproProducts = new HashMap();
		int remove = 0;
		try {
			System.out.println("Reading file..");
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
			String csvFile = inboundRPROFile;
			if (new File(csvFile).exists()) {
				//updated seperator for newline to allow commas in file
				RFC4180Parser rfc4180Parser = new RFC4180ParserBuilder().withSeparator('\n').build();

				CSVReader reader = new CSVReaderBuilder(new FileReader(csvFile)).withCSVParser(rfc4180Parser).build();
				
				List<String[]> rproData = reader.readAll();
				System.out.println("Data read..");
				StringTokenizer tokens = new StringTokenizer(rproData.get(0)[0], delimiter);
				int i = 0;
				while (tokens.hasMoreTokens()) {
					String header = tokens.nextToken();
					if (header.equals("ALU"))
						header = "sku";
					headers.add(i, header);
					i++;
				}
				
				//writer.writeNext(headers.toArray(new String[tokens.countTokens()]));
				System.out.println("Total Products : " + rproData.size());
				for (int j = 1; j < rproData.size(); j++) 
				{
					try {
					int headerIndex = 0;
					List<String> productDataAll = new ArrayList<String>();
					String productAttrVal = null;
					String productAttrPrevVal = null;
					//System.out.println(rproData.get(j)[0]);
					StringTokenizer productData = new StringTokenizer(rproData.get(j)[0], delimiter, true);

					boolean removeProduct = false;

					int upcIndex = headers.indexOf("LOCAL_UPC");
					int flagIndex = headers.indexOf("FLAG");
					int dcsIndex = headers.indexOf("DCS_CODE");
					//System.out.println("DCS_CODE " + dcsIndex);
					
					while (productData.hasMoreTokens()) {
						String header = null;
						
						productAttrVal = productData.nextToken();

						//System.out.println(productAttrVal + "");

						if (headerIndex == upcIndex && (productAttrVal.equals("|") && productAttrPrevVal.equals("|"))) {
							removeProduct = true;
							break;
						}
						//filter for request to remove products with UPC value of 5
						if (headerIndex == upcIndex && (productAttrVal.equals("5"))) {
							removeProduct = true;
							break;
						}
						
						String [] allCheckVal = {"O" , "X" , "N" , "Z" , "U"};
						
						for (String strTmp : allCheckVal) {
							if (headerIndex == dcsIndex && (productAttrVal.startsWith(strTmp))) {
								removeProduct = true;
								break;
							}
						}
						
						if (productAttrVal.equals(delimiter) && productAttrVal.equals(productAttrPrevVal)) {
							header = headers.get(headerIndex);
							productAttrVal = "";
							productDataAll.add(productAttrVal);
							productAttrPrevVal = delimiter;
							headerIndex++;
						} else {
							if (!(productAttrVal.equals(delimiter))) {
								header = headers.get(headerIndex);
								if (headerIndex == upcIndex) {
									String updatedUPC = transformUPC(productAttrVal);
									productAttrVal = updatedUPC;
								}
								if (headerIndex == flagIndex) 
								{
									String updatedFlag = "";
									if(productAttrVal.equals("0")) updatedFlag = "0"; 
									else if (productAttrVal.equals("2")) updatedFlag = "1";
									else updatedFlag = productAttrVal;									
									productAttrVal = updatedFlag;
								}
								productDataAll.add(productAttrVal);
								productAttrPrevVal = productAttrVal;
								headerIndex++;
							} else
								productAttrPrevVal = delimiter;
						}
					}

					if (removeProduct) {
						remove++;
						//System.out.println("Product Removed: "+productDataAll.toString());

					}
					if (!removeProduct) {
//					System.out.println(productDataAll.toString());
						//writer.writeNext(productDataAll.toArray((new String[tokens.countTokens()])));
					}
					
				}
					catch(Exception e)
					{
						e.printStackTrace();
						continue;
					}
				}
				System.out.println("No. of products deleted: " + remove);
				System.out.println("Write complete...");
				//writer.close();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception error) {
			error.printStackTrace();
		}

	}

	public static String transformUPC(String upc) {
		String updatedUPC = upc;
		if (updatedUPC.length() >= 12)
			updatedUPC = upc;
		else {
			int paddingSize = 12;
			updatedUPC = StringUtils.leftPad(updatedUPC, paddingSize, "0");
		}
		return updatedUPC;
	}

}
