# RPRO Inbound Data Processor

This Java Maven project is used to transform the data coming from RPRO before it is imported into Akeneo.

## Build

1. Install Apache Maven

2. In the `InboundDataProcessor` directory run `mvn package`

3. Note the newly created jar file in the `./target` directory

## Deploy

Copy this newly created jar file into `/opt/akeneo_pim/inbound/rpro/jar` on the `api.budgetgolf.com` server